<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
})->name('home')->middleware('auth:web');

// Route for Voyager admin panel
Route::group([
    'prefix' => 'admin',
], function () {
    Voyager::routes();
});

// Social Login
Route::get('login/{source}', 'SocialLoginController@redirectToProvider')->name('login.social')->where('source', 'facebook|google');
Route::get('login/{source}/callback', 'SocialLoginController@handleProviderCallback')->where('source', 'facebook|google');

// Laravel auth
Auth::routes();

