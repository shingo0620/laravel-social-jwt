<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'prefix' => 'auth',
    'namespace' => 'API\Auth',
], function () {
    Route::post('signup', 'AuthController@signup');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');

    // password
    Route::prefix('password')->group(function () {
        Route::post('forget', 'AuthController@forgetPassword');
        Route::post('reset', 'AuthController@resetPassword');
    });

    // social login
    Route::get('sociallogins', 'SocialLoginController@index');
    Route::post('sociallogin/{source}/login', 'SocialLoginController@login')->name('sociallogin.login');
});

Route::get('articles', 'ArticleController@index');