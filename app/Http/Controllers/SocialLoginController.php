<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Socialite;
use Exception;
use App\Exceptions\ReturnException;
use App\SocialLogin;
use App\User;

class SocialLoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($source)
    {
        $socialLogin = SocialLogin::where('name', $source)->firstOrFail();    
        
        switch($source) {
            // supported social login
            case 'facebook':
            case 'google':
                break;

            default:
                throw new ReturnException('Unsupported social login');

        }
        return Socialite::driver($source)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request, $source)
    {
        switch($source) {
            case 'facebook':
            case 'google':
                $socialLoginUser = Socialite::driver($source)->user();
                break;

            default:
                return redirect('/');
        }
        
        $user = User::where('email', $socialLoginUser->email)->first();        
        if($user) {
            // User exist, login and return the JWT token
            Auth::guard('web')->login($user);
            return redirect()->route('home');
        } else {
            // User not exist, register a new user with random password
            $user = User::create([
                'name' => $socialLoginUser->name,
                'email' => $socialLoginUser->email,
                'password' => Hash::make(str_random(32)),
            ]);
            Auth::guard('web')->login($user);
            return redirect()->route('home');
        }
    }
}
