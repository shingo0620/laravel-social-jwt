<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;

// facades
use Illuminate\Support\Facades\Auth;

// models
use App\User;
use App\SocialLogin;

// exceptions
use Exception;
use App\Exceptions\ReturnException;

// 3rd packages
use Socialite;

class SocialLoginController extends Controller
{
    use AuthTrait;

    public function index()
    {
        $responseObj = [
            'data' => array()
        ];
        $socialLogins = SocialLogin::where('enable', true)->get();
        foreach($socialLogins as $socialLogin) {
            $responseObj['data'][] = [
                'type' => 'social_logins',
                'id' => $socialLogin->id,
                'attributes' => [
                    'name' => $socialLogin->name,
                    'redirect_url' => $socialLogin->redirect_url
                ]
            ];
        }
        return response()->json($responseObj);
    }

    /**
     * Login the user with token
     *
     * @return \Illuminate\Http\Response
     */
    public function login($source)
    {
        $socialToken = Request()->token;
        if(Request()->filled('secret')) {
            // OAuth1
            $secret = Request()->secret;
            $socialLoginUser = Socialite::driver($source)->stateless()->userFromTokenAndSecret($socialToken, $secret);
        } else {
            // OAuth2
            $socialLoginUser = Socialite::driver($source)->stateless()->userFromToken($socialToken);
        }
        
        $user = User::where('email', $socialLoginUser->email)->first();        
        if($user) {
            // User exist, login and return the JWT token
            $token = Auth::guard('api')->login($user);
        } else {
            // User not exist, register a new user with random password
            $user = User::create([
                'name' => $socialLoginUser->name,
                'email' => $socialLoginUser->email,
                'password' => Hash::make(str_random(32)),
            ]);
            $token = Auth::guard('web')->login($user);
        }

        return $this->respondWithToken($token);
    }
}
