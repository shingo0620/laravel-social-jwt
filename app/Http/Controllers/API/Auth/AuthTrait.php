<?php

namespace App\Http\Controllers\API\Auth;

trait AuthTrait {
  protected function respondWithToken($token)
  {
    return response()->json([
        'meta' => [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expoires_in' => auth('api')->factory()->getTTL() * 60
        ]
    ]);
  }
}