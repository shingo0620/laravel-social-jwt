<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;

use App\Exceptions\ReturnException;


use App\User;
use App\Http\Resources\UserResource;



class AuthController extends Controller
{
    //
    use AuthTrait;

    public function __construct()
    {
        $this->middleware(
            'auth:api', 
            ['except' => [
                'signup',
                'login',
                'forgetPassword',
                'resetPassword'
            ]]
        );
    }

    public function signup()
    {
        request()->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
        ]);
        $token = Auth::guard('web')->login($user);
        return $this->respondWithToken($token);
    }
    
    public function login()
    {
        $credential = request(['email', 'password']);
        if (! $token = auth('api')->attempt($credential)) {
            throw new ReturnException('Unauthorized', 401);
        }
        return $this->respondWithToken($token);
    }

    public function logout()
    {
        auth('api')->logout();
        return response()->json([
            'meta' => [
                'message' => 'Logout success.'
            ]
        ]);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    public function me()
    {
        return response()->json([
            'data' => new UserResource(auth('api')->user())
        ]);
    }

    public function forgetPassword()
    {
        request()->validate([
            'email' => 'email'
        ]);
        $user = User::where('email', request('email'))->firstOrFail();
        $token = Password::broker()->createToken($user);
        $user->sendPasswordResetNotification($token);

        return response()->json([
            'meta' => [ 
                'message' => 'Reset email sent.'
            ]
        ]);
    }

    public function resetPassword()
    {
        request()->validate([
            'email' => 'required|string|email|max:255',
            'new_password' => 'required|string|min:6|confirmed',
            'token' => 'required|string|max:255',
        ]);
        
        $user = User::where('email', request('email'))->firstOrFail();
        if(Password::getRepository()->exists($user, request('token'))) {
            $user->password = Hash::make(request('new_password'));
            $user->setRememberToken(Str::random(60));
            $user->save();
            event(new PasswordReset($user));
            Password::getRepository()->delete($user);
            return response()->json([
                'meta' => [ 
                    'message' => 'Password reset success.'
                ]
            ]);
        } else {
            throw new ReturnException('Token not reconized', 401);            
        }
        
    }
}
