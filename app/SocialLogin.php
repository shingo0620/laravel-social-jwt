<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Socialite;

class SocialLogin extends Model
{
    public $timestamps = false;
    public $additional_attributes = ['check_status'];
    protected $fillable = [
        'name'
    ];

    /**
     * Check the environment to get this social login is setting properly.
     * In .env file, the social login settings should look like 
     * FACEBOOK_CLIENT_ID=xxxxx and FACEBOOK_CLIENT_SECRET=xxxxx
     * 
     * @return boolean
     */
    public function getCheckStatusAttribute()
    {
        $idKey = strtoupper($this->name) . "_CLIENT_ID";
        $secretKey = strtoupper($this->name) . "_CLIENT_SECRET";
        $redirectKey = strtoupper($this->name) . "_LOGIN_REDIRECT";
        return env($idKey) && env($secretKey) && env($redirectKey);
    }

    public function getRedirectUrlAttribute() {
        return $this->check_status ? Socialite::driver($this->name)->stateless()->redirect()->getTargetUrl() : null;
    }
}
