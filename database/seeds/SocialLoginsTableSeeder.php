<?php

use Illuminate\Database\Seeder;

// models
use App\SocialLogin;
use App\Exceptions\VoyagerDataTypeExistException;
use App\Exceptions\VoyagerPermissionExistException;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class SocialLoginsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'social_logins';
        if(DB::table($tableName)->count() === 0) {
            // insert for social_logins table
            $socialLoginNames = ['facebook', 'google', 'twitter', 'github'];
            foreach($socialLoginNames as $socialLoginName){
                SocialLogin::create([
                    'name' => $socialLoginName,
                    'enable' => false
                ]);
            }
        }

        // insert for Voyager data_types table
        $dataTypeSetting = new stdClass();
        $dataTypeSetting->name                  = $tableName;
        $dataTypeSetting->slug                  = 'social-logins';
        $dataTypeSetting->display_name_singular = 'Social Login';
        $dataTypeSetting->display_name_plural   = 'Social Logins';
        $dataTypeSetting->model_name            = SocialLogin::class;
        $dataTypeSetting->generate_permissions  = true;
        $dataTypeSetting->server_side           = false;
        $dataTypeSetting->controller            = null;
        $dataType = VoyagerSeederHelper::generateDataType($dataTypeSetting);

        // insert for Voyager data_rows table
        try {
            $fields   = 
                ['data_type_id', 'field' , 'type'           , 'display_name', 'required', 'browse', 'read', 'edit', 'add', 'delete', 'order', 'details'];
            $settings = [
                [$dataType->id , 'id'    , 'text'           , 'Id'          , 1         , 1       , 0     , 0     , 0    , 0       , 1      , null],
                [$dataType->id , 'name'  , 'select_dropdown', 'Name'        , 1         , 1       , 1     , 1     , 1    , 1       , 2      , '{"default":"facebook","options":{"facebook":"facebook","twitter":"twitter","google":"google","github":"github"}}'],
                [$dataType->id , 'enable', 'checkbox'       , 'Enable'      , 1         , 1       , 1     , 1     , 1    , 1       , 3      , '{"on":"Enable","off":"Disable"}'],
            ];
            $dataRowSettings = array();
            foreach($settings as $setting) {
                $settingObj = new stdClass();
                foreach($fields as $key=>$field) {
                    $settingObj->$field = $setting[$key];
                }
                $dataRowSettings[] = $settingObj;
            }
            VoyagerSeederHelper::generateDataRows($dataType->id, $dataRowSettings);
        } catch (VoyagerDataTypeExistException $e) {
            echo $e->getMessage();
        }
        
        // insert for Voyager permissions table
        try {
            if($dataTypeSetting->generate_permissions) {
                VoyagerSeederHelper::generatePermission($tableName);
            }
        } catch (VoyagerPermissionExistException $e) {
            echo $e->getMessage();
        }

        // insert for Voyager menu items table
        try {
            $menu = Menu::where('name', 'admin')->firstOrFail();
            $menuItem = MenuItem::firstOrNew([
                'menu_id' => $menu->id,
                'title'   => 'Social Logins',
                'url'     => '',
                'route'   => 'voyager.social-logins.index',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-twitter',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 15,
                ])->save();
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
