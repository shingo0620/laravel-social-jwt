<?php

use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;
use Arrilot\Widgets\Test\Dummies\Exception;
use App\Exceptions\VoyagerDataTypeExistException;
use App\Exceptions\VoyagerPermissionExistException;

class VoyagerSeederHelper
{
  
  // insert for Voyager data_types table
  static public function generateDataType($setting)
  {
    $dataType = DataType::firstOrNew([
      'name' => $setting->name
    ], [
      'name' => $setting->name,
      'slug' => $setting->slug,
      'display_name_singular' => $setting->display_name_singular,
      'display_name_plural' => $setting->display_name_plural,
      'model_name' => $setting->model_name,
      'generate_permissions' => $setting->generate_permissions,
      'server_side' => $setting->server_side,
      'controller' => $setting->controller,
    ]);
    $dataType->save();
    return $dataType;
  }

  // insert for Voyager data_rows table
  // DataRows are for saving bread settings for spefic table(datatype)
  static public function generateDataRows($dataTypeId, $settings=null)
  {
    if(DataRow::where('data_type_id', $dataTypeId)->count() === 0) {
      foreach($settings as $setting) {
        DataRow::create([
          'data_type_id'  => $setting->data_type_id,
          'field'         => $setting->field,
          'type'          => $setting->type,
          'display_name'  => $setting->display_name,
          'required'      => $setting->required,
          'browse'        => $setting->browse,
          'read'          => $setting->read,
          'edit'          => $setting->edit,
          'add'           => $setting->add,
          'delete'        => $setting->delete,
          'details'       => $setting->details,
          'order'         => $setting->order
        ]);
      }
    } else {
      throw new VoyagerDataTypeExistException("DataRow for data_type_id: $dataTypeId is already exist. Skip generate data rows.\n\r");
    }
  }

  // insert for Voyager permissions table
  static public function generatePermission($tableName)
  {
    if(Permission::where('table_name', $tableName)->count() === 0) {
      $keyPrefixes = ['browse', 'read', 'edit', 'add', 'delete'];
      foreach($keyPrefixes as $keyPrefix) {
          $key = $keyPrefix.'_'.$tableName;
          $premission = Permission::create([
              'key'           => $key,
              'table_name'    => $tableName
          ]);
      }
      
      // sync for permission_role
      $role = Role::where('name', 'admin')->firstOrFail();
      $permissions = Permission::all();
      $role->permissions()->sync(
          $permissions->pluck('id')->all()
      );
    } else {
      throw new VoyagerPermissionExistException("Permissions for $tableName is already exist. Skip generate permissions.\n\r");
    }
  }
}