<?php

use Illuminate\Database\Seeder;
use App\Article;
use App\Exceptions\VoyagerDataTypeExistException;
use App\Exceptions\VoyagerPermissionExistException;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableName = 'articles';
        // sample data for articles
        // if(env('APP_DEBUG', false) === true && DB::table($tableName)->count() === 0) {
        //     for($i=0; $i<20; $i++) {
        //         $article = factory(App\Article::class)->make();
        //         $article->save();
        //     }
        // }

        // insert for Voyager data_types table
        $dataTypeSetting = new stdClass();
        $dataTypeSetting->name = $tableName;
        $dataTypeSetting->slug = 'articles';
        $dataTypeSetting->display_name_singular = 'Article';
        $dataTypeSetting->display_name_plural = 'Articles';
        $dataTypeSetting->model_name = App\Article::class;
        $dataTypeSetting->generate_permissions = true;
        $dataTypeSetting->server_side = false;
        $dataTypeSetting->controller = 'Bread\ArticleController';
        $dataType = VoyagerSeederHelper::generateDataType($dataTypeSetting);

        // insert for Voyager data_rows table
        try {
            $fields   = 
                ['data_type_id', 'field'                                , 'type'           , 'display_name' , 'required', 'browse', 'read', 'edit', 'add', 'delete', 'order', 'details'];
            $settings = [
                [$dataType->id , 'id'                                   , 'text'           , 'Id'           , 1         , 1       , 1     , 0     , 0    , 0       , 1      , null],
                [$dataType->id , 'title'                                , 'text'           , 'Title'        , 1         , 1       , 1     , 1     , 1    , 1       , 4      , null],
                [$dataType->id , 'body'                                 , 'rich_text_box'  , 'Body'         , 1         , 0       , 1     , 1     , 1    , 1       , 5      , null],
                [$dataType->id , 'slug'                                 , 'text'           , 'Slug'         , 1         , 1       , 1     , 1     , 1    , 1       , 6      , null],
                [$dataType->id , 'user_id'                              , 'text'           , 'User Id'      , 1         , 0       , 0     , 0     , 0    , 0       , 2      , null],
                [$dataType->id , 'last_modifier'                        , 'text'           , 'Last Modifier', 0         , 0       , 0     , 0     , 0    , 0       , 3      , null],
                [$dataType->id , 'created_at'                           , 'timestamp'      , 'Created At'   , 0         , 1       , 1     , 1     , 0    , 0       , 7      , null],
                [$dataType->id , 'updated_at'                           , 'timestamp'      , 'Updated At'   , 0         , 1       , 1     , 0     , 0    , 0       , 8      , null],
                [$dataType->id , 'article_belongsto_user_relationship'  , 'relation'       , 'Author'       , 0         , 0       , 1     , 1     , 1    , 1       , 9      , '{"model":"App\\User","table":"users","type":"belongsTo","column":"user_id","key":"id","label":"name","pivot_table":"articles","pivot":"0","taggable":"0"}'],
                [$dataType->id , 'article_belongsto_user_relationship_1', 'relation'       , 'Last Modifier', 0         , 0       , 1     , 0     , 0    , 0       , 10     , '{"model":"App\\User","table":"users","type":"belongsTo","column":"last_modifier","key":"id","label":"name","pivot_table":"articles","pivot":"0","taggable":"0"}'],
            ];
            $dataRowSettings = array();
            foreach($settings as $setting) {
                $settingObj = new stdClass();
                foreach($fields as $key=>$field) {
                    $settingObj->$field = $setting[$key];
                }
                $dataRowSettings[] = $settingObj;
            }
            VoyagerSeederHelper::generateDataRows($dataType->id, $dataRowSettings);
        } catch (VoyagerDataTypeExistException $e) {
            echo $e->getMessage();
        }

        // insert for Voyager permissions table
        try {
            if($dataTypeSetting->generate_permissions) {
                VoyagerSeederHelper::generatePermission($tableName);
            }
        } catch (VoyagerPermissionExistException $e) {
            echo $e->getMessage();
        }

        // insert for Voyager menu items table
        try {
            $menu = Menu::where('name', 'admin')->firstOrFail();
            $menuItem = MenuItem::firstOrNew([
                'menu_id' => $menu->id,
                'title'   => 'Articles',
                'url'     => '',
                'route'   => 'voyager.articles.index',
            ]);
            if (!$menuItem->exists) {
                $menuItem->fill([
                    'target'     => '_self',
                    'icon_class' => 'voyager-file-text',
                    'color'      => null,
                    'parent_id'  => null,
                    'order'      => 16,
                ])->save();
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
