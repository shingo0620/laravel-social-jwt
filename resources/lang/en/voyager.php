<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Panel Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for voyager admin panel.
    |
    */

    'check_status' => 'Check Status',
];
