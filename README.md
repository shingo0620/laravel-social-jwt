# Laravel-Starter
A starter template for every CMS-like project.

## Intrgrated with:<br>
- laravel/framework 5.7.*
- tymon/jwt-auth 1.0.0-rc.3
- laravel/socialite ^3.0
- tcg/voyager: ^1.1

## How to start:
* Clone this project, prepare the environment for php ^7.1.3<br>
* Install all dependencies<br>
$ `composer install`<br>
* Copy the .env.example file into .env, generate key for your app and jwt<br>
$ `cp .env.example .env`<br>
$ `php artisan key:generate`<br>
$ `php artisan jwt:secret`<br>
* Install for Voyager<br>
$ `php artisan voyager:install`<br>
* Create the first admin user for Voyager<br>
$ `php artisan voyager:admin your@email.com --create`<br>
* Config your .env file, remember to setup your **social login tokens**.

## Seeding for Voyager setting (optional)
`php artisan db:seed`<br>


## Access with web route
The routes with auth:web middleware would use session guard.

## Access with API route
The routes with auth:api middleware would use api guard with jwt drier

## Setup for facebook login
* Setup app in [facebook development](https://developers.facebook.com/apps)
* Create a product for facebook login

## Setup for google login
* Enable the **Google+ API** for your google API project in the [API console](https://console.developers.google.com/apis/library)
* Setup the id and secret with this [instruction](https://developers.google.com/identity/sign-in/web/)

## More information & reference
* [JWT-auth](http://jwt-auth.readthedocs.io/en/develop/)
* [Voyager: install and create user](https://voyager.readme.io/docs/installation)
* [Socialite](https://laravel.com/docs/5.6/socialite)

